<?php
/**
 * @file
 * Deprecated functions.
 */

// phpcs:disable Drupal.Commenting.Deprecated,Drupal.Semantics.FunctionTriggerError

/**
 * Preprocess arch checkout summary theme.
 *
 * @param array $variables
 *   Variables array.
 *
 * @deprecated
 */
function arch_checkout_preprocess_arch_checkout_op_summary(array &$variables) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_onepage_preprocess_arch_checkout_op_summary', E_USER_DEPRECATED);
  arch_onepage_preprocess_arch_checkout_op_summary($variables);
}
