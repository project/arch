<?php

namespace Drupal\arch_shipping;

use Drupal\arch_order\Entity\OrderInterface;

/**
 * Add extra line item on checkout.
 */
interface ShippingMethodExtraLineItemInterface {

  /**
   * Add extra line item on checkout.
   *
   * @param \Drupal\arch_order\Entity\OrderInterface $order
   *   Order entity.
   */
  public function addExtraLineItem(OrderInterface $order);

  /**
   * Get extra price.
   *
   * @param \Drupal\arch_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return \Drupal\arch_price\Price\PriceInterface
   *   Price.
   */
  public function getExtraPrice(OrderInterface $order);

}
