<?php

namespace Drupal\arch_shipping;

use Drupal\arch_order\Entity\OrderInterface;

/**
 * Shipping method interface.
 *
 * @package Drupal\arch_shipping
 */
interface ShippingPriceLabelInterface extends ShippingMethodInterface {

  /**
   * Create price label for checkout.
   *
   * @param \Drupal\arch_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Label.
   */
  public function getShippingPriceLabel(OrderInterface $order);

  /**
   * Create description for checkout.
   *
   * @param \Drupal\arch_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return null|string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Description.
   */
  public function getShippingMethodDescription(OrderInterface $order);

}
