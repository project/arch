<?php

namespace Drupal\arch_addressbook\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides local action definitions for addressbook items.
 */
class ArchAddressbookLocalAction extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $this->derivatives['addressbookitem.add_to_user'] = [
      'route_name' => 'addressbookitem.add_to_user',
      'title' => $this->t('Add new address', [], [
        'context' => 'arch_addressbook',
      ]),
      'appears_on' => [
        'view.addressbook.my_addresses',
      ],
    ];
    return $this->derivatives;
  }

}
