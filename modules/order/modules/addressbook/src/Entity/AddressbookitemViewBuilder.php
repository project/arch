<?php

namespace Drupal\arch_addressbook\Entity;

use Drupal\arch_addressbook\AddressbookitemInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Render\Element\Link;

/**
 * View builder handler for Addressbookitems.
 */
class AddressbookitemViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    if (empty($entities)) {
      return;
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];

      if ($display->getComponent('links')) {
        $build[$id]['links'] = [
          '#lazy_builder' => [
            static::class . '::renderLinks', [
              $entity->id(),
              $view_mode,
              $entity->language()->getId(),
              !empty($entity->in_preview),
              $entity->isDefaultRevision() ? NULL : $entity->getLoadedRevisionId(),
            ],
          ],
        ];
      }

      if ($display->getComponent('langcode')) {
        $build[$id]['langcode'] = [
          '#type' => 'item',
          '#title' => $this->t('Language', [], ['context' => 'arch_addressbook']),
          '#markup' => $entity->language()->getName(),
          '#prefix' => '<div id="field-language-display">',
          '#suffix' => '</div>',
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $defaults = parent::getBuildDefaults($entity, $view_mode);

    if (isset($defaults['#cache']) && isset($entity->in_preview)) {
      unset($defaults['#cache']);
    }

    return $defaults;
  }

  /**
   * Callback for #lazy_builder callback; builds a addressbookitem's links.
   *
   * @param string $entity_id
   *   The entity ID.
   * @param string $view_mode
   *   The view mode in which the entity is being viewed.
   * @param string $langcode
   *   The language in which the entity is being viewed.
   * @param bool $is_in_preview
   *   Whether the entity is currently being previewed.
   * @param int|null $revision_id
   *   (optional) The identifier of the entity revision to be loaded. If none
   *   is provided, the default revision will be loaded.
   *
   * @return array
   *   A renderable array representing the entity links.
   */
  public static function renderLinks($entity_id, $view_mode, $langcode, $is_in_preview, $revision_id = NULL) {
    $links = [
      '#theme' => 'links',
      '#pre_render' => [[Link::class, 'preRenderLinks']],
      '#attributes' => ['class' => ['links', 'inline']],
    ];

    if (!$is_in_preview) {
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
      $storage = \Drupal::entityTypeManager()->getStorage('addressbookitem');
      /** @var \Drupal\arch_addressbook\AddressbookitemInterface $revision */
      $revision = !isset($revision_id) ? $storage->load($entity_id) : $storage->loadRevision($revision_id);
      $entity = $revision->getTranslation($langcode);
      $links['addressbookitem'] = static::buildLinks($entity, $view_mode);
    }
    return $links;
  }

  /**
   * Build the default links (Read more) for an addresbookitem.
   *
   * @param \Drupal\arch_addressbook\AddressbookitemInterface $entity
   *   The entity object.
   * @param string $view_mode
   *   A view mode identifier.
   *
   * @return array
   *   An array that can be processed by drupal_pre_render_links().
   */
  protected static function buildLinks(AddressbookitemInterface $entity, $view_mode) {
    $links = [];

    // Always display a read more link on teasers because we have no way
    // to know when a teaser view is different than a full view.
    if ($view_mode == 'teaser') {
      $title_stripped = strip_tags($entity->label());
      $links['readmore'] = [
        'title' => t('Read more<span class="visually-hidden"> about @title</span>', [
          '@title' => $title_stripped,
        ]),
        'url' => $entity->toUrl(),
        'language' => $entity->language(),
        'attributes' => [
          'rel' => 'tag',
          'title' => $title_stripped,
        ],
      ];
    }

    return [
      '#theme' => 'links',
      '#links' => $links,
      '#attributes' => ['class' => ['links', 'inline']],
    ];
  }

}
