<?php
/**
 * @file
 * Deprecated functions.
 */

use Drupal\arch_order\Entity\OrderInterface;
use Drupal\Core\Session\AccountInterface;

// phpcs:disable Drupal.Commenting.Deprecated,Drupal.Semantics.FunctionTriggerError

/**
 * Fetches an array of permission IDs granted to the given user ID.
 *
 * The implementation here provides only the universal "all" grant. A produc
 * access module should implement hook_order_grants() to provide a grant list
 * for the user.
 *
 * After the default grants have been loaded, we allow modules to alter the
 * grants array by reference. This hook allows for complex business logic to be
 * applied when integrating multiple order access modules.
 *
 * @param string $op
 *   The operation that the user is trying to perform.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account object for the user performing the operation.
 *
 * @return array
 *   An associative array in which the keys are realms, and the values are
 *   arrays of grants for those realms.
 *
 * @deprecated
 *
 * @see arch_order_access_grants()
 */
function order_access_grants($op, AccountInterface $account) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_order_access_grants', E_USER_DEPRECATED);
  return arch_order_access_grants($op, $account);
}

/**
 * Checks if the current page is the full page view of the passed-in order.
 *
 * @param Drupal\arch_order\Entity\OrderInterface $order
 *   A order entity.
 *
 * @return int|false
 *   The ID of the order if this is a full page view, otherwise FALSE.
 *
 * @deprecated
 *
 * @see arch_order_is_page()
 */
function order_is_page(OrderInterface $order) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_order_is_page', E_USER_DEPRECATED);
  return arch_order_is_page($order);
}
