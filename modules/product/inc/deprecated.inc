<?php
/**
 * @file
 * Deprecated functions.
 */

use Drupal\arch_product\Entity\ProductInterface;
use Drupal\Core\Session\AccountInterface;

// phpcs:disable Drupal.Commenting.Deprecated,Drupal.Semantics.FunctionTriggerError

/**
 * Returns a list of available product type names.
 *
 * This list can include types that are queued for addition or deletion.
 *
 * @return string[]
 *   An array of product type labels, keyed by the product type name.
 *
 * @deprecated
 *
 * @see arch_product_type_get_names()
 */
function product_type_get_names() {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_type_get_names', E_USER_DEPRECATED);
  return arch_product_type_get_names();
}

/**
 * Returns the product type label for the passed product.
 *
 * @param \Drupal\arch_product\Entity\ProductInterface $product
 *   A product entity to return the product type's label for.
 *
 * @return string|false
 *   The product type label or FALSE if the product type is not found.
 *
 * @deprecated
 *
 * @see arch_product_get_type_label()
 */
function product_get_type_label(ProductInterface $product) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_get_type_label', E_USER_DEPRECATED);
  return arch_product_get_type_label($product);
}

/**
 * Updates all products of one type to be of another type.
 *
 * @param string $old_id
 *   The current product type of the products.
 * @param string $new_id
 *   The new product type of the products.
 *
 * @return int
 *   The number of products whose product type field was modified.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 *
 * @deprecated
 *
 * @see arch_product_type_update_products()
 */
function product_type_update_products($old_id, $new_id) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_type_update_products', E_USER_DEPRECATED);
  return arch_product_type_update_products($old_id, $new_id);
}

/**
 * Checks if the current page is the full page view of the passed-in product.
 *
 * @param Drupal\arch_product\Entity\ProductInterface $product
 *   A product entity.
 *
 * @return int|false
 *   The ID of the product if this is a full page view, otherwise FALSE.
 *
 * @deprecated
 *
 * @see product_is_page()
 */
function product_is_page(ProductInterface $product) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_is_page', E_USER_DEPRECATED);
  return arch_product_is_page($product);
}

/**
 * Finds the most recently changed products that are available to the user.
 *
 * @param int $number
 *   (optional) The maximum number of products to find. Defaults to 10.
 *
 * @return \Drupal\arch_product\Entity\ProductInterface[]
 *   An array of product entities or an empty array if there are no recent
 *   products visible to the current user.
 *
 * @deprecated
 *
 * @see arch_product_get_recent()
 */
function product_get_recent($number = 10) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_get_recent', E_USER_DEPRECATED);
  return arch_product_get_recent($number);
}

/**
 * Fetches an array of permission IDs granted to the given user ID.
 *
 * The implementation here provides only the universal "all" grant. A produc
 * access module should implement hook_product_grants() to provide a grant list
 * for the user.
 *
 * After the default grants have been loaded, we allow modules to alter the
 * grants array by reference. This hook allows for complex business logic to be
 * applied when integrating multiple product access modules.
 *
 * @param string $op
 *   The operation that the user is trying to perform.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account object for the user performing the operation.
 *
 * @return array
 *   An associative array in which the keys are realms, and the values are
 *   arrays of grants for those realms.
 *
 * @deprecated
 *
 * @see arch_product_access_grants()
 */
function product_access_grants($op, AccountInterface $account) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_access_grants', E_USER_DEPRECATED);
  return arch_product_access_grants($op, $account);
}

/**
 * Determines whether the user has a global viewing grant for all products.
 *
 * Checks to see whether any module grants global 'view' access to a user
 * account; global 'view' access is encoded in the {arch_product_access} table
 * as a grant with pid=0. If no product access modules are enabled, arch.module
 * defines such a global 'view' access grant.
 *
 * This function is called when a product listing query is tagged with
 * 'product_access'; when this function returns TRUE, no product access joins
 * are added to the query.
 *
 * @param \Drupal\Core\Session\AccountInterface|null $account
 *   (optional) The user object for the user whose access is being checked. If
 *   omitted, the current user is used. Defaults to NULL.
 *
 * @return bool
 *   TRUE if 'view' access to all products is granted, FALSE otherwise.
 *
 * @deprecated
 *
 * @see arch_product__product_access_view_all_products()
 * @see hook_product_grants()
 * @see arch_product_query_product_access_alter()
 */
function product_access_view_all_products($account = NULL) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use product_access_view_all_products', E_USER_DEPRECATED);
  return arch_product__product_access_view_all_products($account);
}

/**
 * Toggles or reads values of a flag for rebuilding the product access grants.
 *
 * When the flag is set, a message is displayed to users with 'access
 * administration pages' permission, pointing to the 'rebuild' confirm form.
 * This can be used as an alternative to direct arch_product_access_rebuild
 * calls, allowing administrators to decide when they want to perform the actual
 * (possibly time consuming) rebuild.
 *
 * When unsure if the current user is an administrator,
 * arch_product_access_rebuild() should be used instead.
 *
 * @param bool|null $rebuild
 *   (optional) The boolean value to be written.
 *
 * @return bool|null
 *   The current value of the flag if no value was provided for $rebuild. If a
 *   value was provided for $rebuild, nothing (NULL) is returned.
 *
 * @deprecated
 * @see arch_product__product_access_needs_rebuild()
 */
function product_access_needs_rebuild($rebuild = NULL) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product__product_access_needs_rebuild', E_USER_DEPRECATED);
  return arch_product__product_access_needs_rebuild($rebuild);
}

/**
 * Rebuilds the product access database.
 *
 * This rebuild is occasionally needed by modules that make system-wide changes
 * to access levels. When the rebuild is required by an admin-triggered action
 * (e.g module settings form), calling
 * arch_product__product_access_needs_rebuild(TRUE) instead of
 * arch_product_access_rebuild() lets the user perform their changes and
 * actually rebuild only once they are done.
 *
 * @param bool $batch_mode
 *   (optional) Set to TRUE to process in 'batch' mode, spawning processing over
 *   several HTTP requests (thus avoiding the risk of PHP timeout if the site
 *   has a large number of products). hook_update_N() and any form submit
 *   handler are safe contexts to use the 'batch mode'. Less decidable cases
 *   (such as calls from hook_user(), hook_taxonomy(), etc.) might consider
 *   using the non-batch mode. Defaults to FALSE.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 *
 * @deprecated
 *
 * @see arch_product_access_rebuild()
 */
function product_access_rebuild($batch_mode = FALSE) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product_access_rebuild', E_USER_DEPRECATED);
  arch_product_access_rebuild($batch_mode);
}

/**
 * Implements callback_batch_operation().
 *
 * Performs batch operation for arch_product_access_rebuild().
 *
 * This is a multistep operation: we go through all products by packs of 20. The
 * batch processing engine interrupts processing and sends progress feedback
 * after 1 second execution time.
 *
 * @param array $context
 *   An array of contextual key/value information for rebuild batch process.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 *
 * @deprecated
 *
 * @see arch_product__product_access_rebuild_batch_operation()
 */
function _product_access_rebuild_batch_operation(array &$context) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product__product_access_rebuild_batch_operation', E_USER_DEPRECATED);
  arch_product__product_access_rebuild_batch_operation($context);
}

/**
 * Implements callback_batch_finished().
 *
 * Performs post-processing for arch_product_access_rebuild().
 *
 * @param bool $success
 *   A boolean indicating whether the re-build process has completed.
 * @param array $results
 *   An array of results information.
 * @param array $operations
 *   An array of function calls (not used in this function).
 *
 * @deprecated
 *
 * @see arch_product__product_access_rebuild_batch_finished()
 */
function _product_access_rebuild_batch_finished($success, array $results, array $operations) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use arch_product__product_access_rebuild_batch_finished', E_USER_DEPRECATED);
  arch_product__product_access_rebuild_batch_finished($success, $results, $operations);
}

/**
 * Marks a product to be re-indexed by the product_search plugin.
 *
 * @param int $pid
 *   The product ID.
 *
 * @deprecated
 *
 * @see arch_product_reindex_product_search()
 */
function product_reindex_product_search($pid) {
  @trigger_error(__FUNCTION__ . ' is deprecated in 1.0.0-alpha20 and is removed from 1.0.0. Use product_reindex_product_search', E_USER_DEPRECATED);
  arch_product_reindex_product_search($pid);
}
